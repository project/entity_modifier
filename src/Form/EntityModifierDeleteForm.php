<?php

namespace Drupal\entity_modifier\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Entity modifier entities.
 *
 * @ingroup entity_modifier
 */
class EntityModifierDeleteForm extends ContentEntityDeleteForm {


}
