<?php

namespace Drupal\entity_modifier;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a listing of Entity modifier type entities.
 */
class EntityModifierTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Entity modifier type');
    $header['id'] = $this->t('Machine name');
    $header['group'] = $this->t('Group');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    // Group field.
    $group = $entity->get('group');
    $group_term = !empty($group) ? Term::load($group) : '';
    $row['group'] = empty($group_term) ? $this->t('General'): $group_term->getName();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
