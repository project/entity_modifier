<?php

namespace Drupal\entity_modifier\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Entity modifier type entities.
 */
interface EntityModifierTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
