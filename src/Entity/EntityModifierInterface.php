<?php

namespace Drupal\entity_modifier\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Entity Modifier entities.
 *
 * @ingroup entity_modifier
 */
interface EntityModifierInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Entity Modifier name.
   *
   * @return string
   *   Name of the Entity Modifier.
   */
  public function getName();

  /**
   * Sets the Entity Modifier name.
   *
   * @param string $name
   *   The Entity Modifier name.
   *
   * @return \Drupal\entity_modifier\Entity\EntityModifierInterface
   *   The called Entity Modifier entity.
   */
  public function setName($name);

  /**
   * Gets the Entity Modifier creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Entity Modifier.
   */
  public function getCreatedTime();

  /**
   * Sets the Entity Modifier creation timestamp.
   *
   * @param int $timestamp
   *   The Entity Modifier creation timestamp.
   *
   * @return \Drupal\entity_modifier\Entity\EntityModifierInterface
   *   The called Entity Modifier entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Entity Modifier published status indicator.
   *
   * Unpublished Entity Modifier are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Entity Modifier is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Entity Modifier.
   *
   * @param bool $published
   *   TRUE to set this Entity Modifier to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\entity_modifier\Entity\EntityModifierInterface
   *   The called Entity Modifier entity.
   */
  public function setPublished($published);

}
